#Nota 10


#Atividades

#10.1
lista = []

for i in range(1, 21):
    print(i)

#10.2
lista = []

for i in range(1, 1000001):
    print(i)


#10.3
num = list(range(1, 1000001))

print(" Menor valor:", min(num))
print(" Maior valor:", max(num))
print(" Soma dos valores:", sum(num))


#10.4
for i in range(1,21,2):
    print("\n", i)

#10.5
for i in range(3,1001,3):
    print("\n", i)

#10.6
cubos = []

for num in range(1, 101):
    cubos.append(num ** 3)
    
for i in cubos:
    print("\n", i)

#10.7
cubos = [num**3 for num in range(1, 101)]
    
for i in cubos:
    print("\n", i)
