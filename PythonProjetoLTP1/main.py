'''from Entidades.cachorro import Dog
from Entidades.beagle import Beagle

car = Dog()
car.tamanho = 0.45
car.raca = "Beagle"
car.cor = "Bicolor"
car.peso = 17.0

print(vars(car))

car.latir()
car.comer()'''

from Menu.menu         import Menu
from Dados.dados       import Dados
from Entidades.terra   import Terra 
# from Entidades.saturno import Saturno

d = Dados()

terrinha = Terra()

terrinha.massa        = 5972.0
terrinha.raio         = 6371.0
terrinha.cor          = "Azul"
terrinha.material     = "Rochoso"

d.inserir(terrinha)
print("\n")
print(vars(terrinha))

print("\n")

retorno = d.buscarPorIdentificador(1)
retorno = d.buscarpPorAtributo(6371.0)
print(vars(retorno))

print("\n\n\n")

# saturninho = Saturno()

# saturninho.massa      = 6982.0
# saturninho.raio       = 9987.0
# saturninho.cor        = "marrom"
# saturninho.material   = "gasoso"

# d.inserir(saturninho)
# print(vars(saturninho))

# retornoDois = d.buscarPorIdentificador(2)
# retornoDois = d.buscarpPorAtributo(9987.0)
# print(vars(retornoDois))

# print("\n")

Menu.iniciarMenu()