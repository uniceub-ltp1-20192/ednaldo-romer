from Entidades.planeta import Planeta

class Saturno(Planeta):
    def __init__(self, agua = "não possui"):
        self._agua = agua

    @property
    def agua(self):
        return self._agua

    @agua.setter
    def agua(self,agua):
        self._agua = agua 

    def habitantes(self):
        print("não existem")