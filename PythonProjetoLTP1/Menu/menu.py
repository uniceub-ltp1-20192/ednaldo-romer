from Validador.validador import Validador
from Dados.dados         import Dados
from Entidades.terra     import Terra

class Menu: 

    @staticmethod
    def menuPrincipal():
        print("""
            0 - Sair
            1 - Consultar
            2 - Inserir
            3 - Alterar
            4 - Deletar
             """)
        return Validador.validarOpMenu("[0-4]")     

    @staticmethod
    def menuConsultar():
        print("""
            0 - Sair
            1 - Consultar por identificador
            2 - Consultar por propriedades
            """)
        return Validador.validarOpMenu("[0-2]")

    @staticmethod
    def menuBuscaPorIdentificador(d):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        return retorno



    @staticmethod
    def menuBuscaPorAtributo(d):
        retorno = d.buscarpPorAtributo(Validador.verificarInteiro()) 
        print("menu")

    def menuInserir(self, d):
        t = Terra()
        print("Planeta ")
        t.massa =      input("Insira um valor para massa: ")
        t.raio =       input("Insira um valor referente ao raio: ")
        t.cor =        input("Insira um valor referente a cor: ")
        t.material =   input("Insira o tipo de material de um planeta: ")
        t.habitantes = input("Insira o nome dados para habitantes de um planeta especifico: ")
        t.agua =       input("Possui agua? ")
        d.inserir(t)

    @staticmethod
    def menuAlterar(self, retorno, d):
        print("Planeta ")
        retorno.massa =      Validador.validarValorInformado(retorno.massa,"Insira um valor para massa: ")
        retorno.raio =       Validador.validarValorInformado(retorno.raio,"Insira um valor referente ao raio: ")
        retorno.cor =        Validador.validarValorInformado(retorno.cor,"Insira um valor referente a cor: ")
        retorno.material =   Validador.validarValorInformado(retorno.material,"Insira o tipo de material de um planeta: ")
        retorno.habitantes = Validador.validarValorInformado(retorno.habitantes,"Insira o nome dados para habitantes de um planeta especifico: ")
        retorno.agua =       Validador.validarValorInformado(retorno.agua,"Possui agua? ")
        d.alterar(retorno)

    def menuDeletar(self, retorno,d):
        print(retorno)
        deletar = input("""
        Deseja deletar o registro?
        S - sim
        """)

        if deletar == "S" or deletar == "s":
            d.deletar(retorno)
            print("""
            Seu registro foi deletado
            """)
        else:
            print("Registro nao foi deletado! ")  

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()

            if  opMenu == "1":
                print("Você entrou em consulta! ")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        print("Você entrou em consultar por identificador! ")
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if(retorno != None):
                            print(retorno)
                        else:
                            print("""
                            Nao existem registros com este identificador. 
                            """)

                    elif opMenu == "2":
                        print("Você entrou em consultar por propriedades! ")
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if (retorno != None):
                            print(retorno)
                        else:
                            print("Não foi encontrado nenhum registro com este atributo.")
                    elif opMenu == "0":
                        print("Você voltou ao menu inicial! ")
                opMenu = ""


            elif opMenu == "2":
                print("Você entrou em inserir! ")
                Menu.menuInserir(d)

            elif opMenu == "3":
                print("Você entrou em alterar! ")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()

                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno, d)
                    elif opMenu == "2":
                        print("Você entrou em alterar por propriedades! ")
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno, d)
                    elif opMenu == "0":
                        print("Você voltou ao menu inicial! ")

                opMenu = ""


            elif opMenu == "4":
                print("Você entrou em deletar! ")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()

                    if opMenu == "1":
                        print("Você entrou em deletar por identificador! ")
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno, d)

                    elif opMenu == "2":
                        print("Você entrou em deletar por propriedades! ")
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno, d)
                    elif opMenu == "0":
                        print("Você voltou ao menu inicial! ")

                opMenu = ""
            elif opMenu == "0":
                print("Você saiu do menu! ")
            else:
                print("Informe uma opcao dentre as apresentadas!!! ")


        