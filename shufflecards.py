import itertools, random

#define o que há na lista 'deck'
deck = list(itertools.product(range(0,13),['Espadas','Copas','Ouros','Paus']))

#embaralha o deck
random.shuffle(deck)

#printa as três cartas
print("Suas cartas são: ")
for i in range(0, 3):
   print(deck[i][0], "de", deck[i][1])