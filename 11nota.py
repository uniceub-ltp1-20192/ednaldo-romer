#Nota 11


#Atividades

#11.1
lista = ['arara', 'asa', 'civic', 'aranha', 'carro', 'fada', 'chave', 'ana', 'biscoito']
palavra = input("\n Insira uma palavra para checar se é anagrama de uma \n dessas palavras (arara, asa, civic, chave, ana, biscoito): ")

for x in range (6):
    if sorted(palavra) == sorted(lista[x]):
        print("\n\tSim, esta palavra é um anagrama de", lista[x].title(), "!")
        
print("\n\n Os três primeiros itens da lista são: ", lista[:3])
print(" Os três últimos itens da lista são: ", lista[6:9])
print(" Os três itens do  meio da lista são: ", lista[3:6])

#11.2
pizza_hut          = ['portuguesa', 'calabresa', 'pepperoni']
pizza_hut_paraguai = pizza_hut[:]

print(pizza_hut)
print(pizza_hut_paraguai)

pizza_hut.append('quatro_queijos')
pizza_hut_paraguai.append('marquerita')

print(pizza_hut)
print(pizza_hut_paraguai)


#11.3
pizza_hut = ['portuguesa', 'calabresa', 'pepperoni', 'marguerita', 'chocolate', 'banana']
precos    = ['25,90']

print("\n\t\t Cardápio \n") 
for x in pizza_hut:
    for y in precos:
        print("  ->", x.title(), " --- ", y)
