#Nota 5


#Atividades

#5.1
   num = int(input("Insira um número: "))

   if num % 2 == 0 :
       print("Número par!")
   else :
       print("Número ímpar!")

#5.2

#5.3
num1 = int(input("Insira um número: "))
num2 = int(input("Insira outro número: "))

soma = num1 + num2
sub  = num1 - num2
mult = num1 * num2
div  = num1 / num2

print ("\nResultados: ", "\n\tAdição: ", soma, "\n\tSubtração: ", sub, "\n\tMultiplicação: ", mult, "\n\tDivisão: ", div )

#5.4
sem   = int(input("Em qual semestre você está? "))

anos_faltam = (8 - sem)/2

print ("Faltam ", anos_faltam, " anos para você se formar! \n\tA não ser que você faça medicina...")

#5.5
sem   = int(input("Em qual semestre você está? "))

anos_faltam = (8 - sem)/2

print ("\nFaltam", anos_faltam, "anos para você se formar! \n\tA não ser que você faça medicina... \n\tOu que tenha algum semestre atrasado...")

atraso = float(input("\nTem algum semestre atrasado? \nSe sim, quantos? "))
if atraso > 0:
    print ("\nEntão, seu tempo na faculdade é um pouco maior... \n\tAinda faltam", anos_faltam + atraso/2, "anos!")
