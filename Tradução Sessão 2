Tradução – Sessão 2 – Aula 28/08/2019 – Professor Caio

2.

2.1. Linguagens Estaticamente “Tipadas”
Uma linguagem é dita como “segura” se esta não produz erros de execução que
passam despercebidos e que causam comportamento arbitrário posteriormente,
seguindo a ideia de que programas “bem escritos” não deveriam dar errado.
“Linguagens estaticamente tipadas” asseguram a segurança ao se escrever um código
por meio de sistemas de escrita estática. Entretanto, esses sistemas de escrita não
compilam algumas expressões que não produzem nenhum erro de escrita no
momento de rodar o programa. Isso acontece porque os sistemas de escrita estática
exigem a certeza de que as expressões compiladas não gerem nenhum tipo de erro de
escrita ao se executar o programa.
A escrita estática é centrada em ter certeza de que nenhum erro de escrita acontece
no momento de execução. É por conta disso que linguagens com escrita estática
possuem uma política pessimista no quesito da compilação do programa. Esse
pessimismo causa erros de compilação em programas que não produzem nenhum erro
de execução. Em contrapartida, linguagens estáticas também permitem a execução de
programas que possam erros na execução.

2.2. Linguagens Dinamicamente “Tipadas”
As linguagens dinâmicas são o oposto das estáticas. Ao invés de ter certeza de que
todas as expressões válidas serão executadas sem erros, elas fazem com que qualquer
programa sintaticamente correto seja compilado. Essa é uma abordagem mais otimista
que causa um alto número de erros de escrita no momento de execução que podem
ter sido detectados no momento da compilação. Essa abordagem permite muitos erros
de escrita no momento de execução, compilando programas que podem ser
identificados como errados numa abordagem estática. Nesse caso, o programa é
compilado, enquanto que, em um sistema de linguagem estática, os erros seriam
detectados antes da execução.