#Nota 12


#Atividades

#12.1
comidas = ('lasanha', 'pizza', 'hamburguer', 'sushi', 'crepe')

print("\n  Comidas: \n")
for comida in comidas:
    print("     ->", comida.title())
    
#comidas[0] = 'escondidinho'

comidas = ('lasanha', 'arroz', 'hamburguer', 'sushi', 'camarão')

print("\n  Comidas: \n")
for comida in comidas:
    print("     ->", comida.title())
