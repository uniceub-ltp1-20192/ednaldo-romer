#Nota 9


#Atividades

#9.1
lista = [1, 5 ,7 ,4 ,9, 13]

soma = sum(lista)

print(soma)

#9.2
import numpy

lista = [1, 5 ,7 ,4 ,9, 13]

result = numpy.prod(lista)
print(result)

#9.3
lista = [5, 1 ,7 ,4 ,9, 13]

print(" Menor valor: ", min(lista))


#9.4
lista_a = [5, 1 ,7 ,4 ,9, 13]
lista_b = [4, 6, 8, 5, 9, 0]

for x in lista_a:
    for y in lista_b:
        if x == y:
            print(x)
            
#9.5
lista = ['honda', 'volkswagen', 'fiat', 'chevrolet', 'ford']

lista.sort()

for x in lista:
    print(x.title())

#9.6
lista = ['paralelepípedo', 'dodecaedro', 'icosaedro']

i = len(lista)

for x in range (i):
    print(len(lista[x]))


#9.7
lista = ['arara', 'asa', 'civic', 'chave', 'ana', 'biscoito']
palavra = input("\n Insira uma palavra para checar se é anagrama de uma \n dessas palavras (arara, asa, civic, chave, ana, biscoito): ")

for x in range (6):
    if sorted(palavra) == sorted(lista[x]):
        print("\n\tSim, esta palavra é um anagrama de", lista[x].title(), "!")

