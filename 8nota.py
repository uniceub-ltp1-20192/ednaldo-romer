#Nota 8


#Atividades

#8.1
places = ['tokyo', 'toronto', 'amsterdam', 'orlando', 'roma']

print("\nLista original: \n", places)
print("\nLista em ordem alfabética: \n", sorted(places))
print("\nLista original novamente: \n", places)

print("\n\nLista em ordem alfabética reversa: \n", sorted(places, reverse=True))
print("\nLista original novamente: \n", places)

places.reverse()
print("\n\nLista invertida de forma permanente: \n", places)

places.sort()
print("\n\nLista agora em ordem alfabética de forma permanente: \n", places)

places.sort(reverse=True)
print("\n\nLista agora em ordem alfabética inversa de forma permanente: \n", places)

#8.2
guests = ['albert', 'thomas', 'isaac']

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("\nCaro", guests[2].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )

print("\n\nQuantidade de convidados: " ,len(guests))

#8.3
music = ['violão', 'guitarra', 'baixo', 'bateria', 'piano', 'viola', 'ukulele']

print(" Lista Default: \n")
for i in music:
    print("  ->", i.title())
    
print("\n\n Lista em Ordem Alfabética Temporária: \n")
print(sorted(music), "\n\n")
    
music.sort()
print(" Lista Ordem Alfabética Permanente: \n")
for i in music:
    print("  ->", i.title())
    
print("\n\n\t\t ...")

print("\n Quando for comprar um instrumento, a ordem de compra será: ")
print("\n  Primeiro:", music[2].title())
print("  Segundo: ", music[5].title())
print("  Terceiro:", music[0].title())
print("  Quarto:  ", music[6].title())
print("  Quinto:  ", music[1].title())
print("  Sexto:   ", music[3].title())
print("  Sétimo:  ", music[4].title())

print("\n\n\t\t ...")

print("\n Já a ordem alfabética ao contrário seria: ")
music.reverse()
for i in music:
    print("  ->", i.title())
    
print("\n\n\t\t ...")

print("\n Depois de tudo isso, percebemos que a lista possui", len(music), "caracteres.")

