#Nota 7


#Atividades

#7.1
guests = ['albert', 'thomas', 'isaac']

print("Caro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("Caro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("Caro", guests[2].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )

#7.2
guests = ['albert', 'thomas', 'isaac']

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("\nCaro", guests[2].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )

print("\n\n\t\t\t...")

missing_guest = guests.pop(2)
print("\n\nInfelizmente, o senhor", missing_guest.title(), "não pôde comparecer, \npor conta disso, enviarei novos convites...")

print("\n\n\t\t\t...")

guests.append('alissa')

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCara", guests[2].title(), ", \nVenho por meio desta mensagem convidá-la para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )

#7.3
guests = ['albert', 'thomas', 'isaac']

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("\nCaro", guests[2].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )

print("\n\n\t\t\t...")

missing_guest = guests.pop(2)
print("\n\nInfelizmente, o senhor", missing_guest.title(), "não pôde comparecer, \npor conta disso, enviarei novos convites...")

print("\n\n\t\t\t...")

guests.append('alissa')

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCara", guests[2].title(), ", \nVenho por meio desta mensagem convidá-la para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )

print("\n\nFelizmente, consegui uma mesa duas vezes maior! \nLogo, convidarei mais três pessoas...")

guests.insert(0, 'ayrton')
guests.insert(2, 'stephen')
guests.append('nikola')

print("\n\n\t\t\t...")

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[2].title(), ", \nVenho por meio desta mensagem convidá-la para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[3].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCara", guests[4].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[5].title(), ", \nVenho por meio desta mensagem convidá-la para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )

#7.4
guests = ['albert', 'thomas', 'isaac']

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )
print("\nCaro", guests[2].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida e suas peculiaridades. \n\tAtensiosamente." )

print("\n\n\t\t\t...")

missing_guest = guests.pop(2)
print("\n\nInfelizmente, o senhor", missing_guest.title(), "não pôde comparecer, \npor conta disso, enviarei novos convites...")

print("\n\n\t\t\t...")

guests.append('alissa')

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCara", guests[2].title(), ", \nVenho por meio desta mensagem convidá-la para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )

print("\n\nFelizmente, consegui uma mesa duas vezes maior! \nLogo, convidarei mais três pessoas...")

guests.insert(0, 'ayrton')
guests.insert(2, 'stephen')
guests.append('nikola')

print("\n\n\t\t\t...")

print("\nCaro", guests[0].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[1].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[2].title(), ", \nVenho por meio desta mensagem convidá-la para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[3].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCara", guests[4].title(), ", \nVenho por meio desta mensagem convidá-lo para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )
print("\nCaro", guests[5].title(), ", \nVenho por meio desta mensagem convidá-la para um jantar casual, \npara discutir os diversos aspectos da vida, como metal e física. \n\tAtensiosamente." )

print("\n\n\t\t\t...")

print("\n\nCaros e cara, \nAparentemente o destino não nos quer \ntodos em uma única mesa, \npois a grande mesa que mencionei não chegará a tempo. \nTerei espaço para apenas dois convidados... \nPerdão a todos...")


print("\n\n\t\t", guests)

pop1 = guests.pop(0)
print("\n\nDesculpe-me", pop1.title(), ",seu convite foi cancelado. \nObrigado pela paciência...")
print("\n\t\t", guests)

pop2 = guests.pop(1)
print("\n\nDesculpe-me", pop2.title(), ",seu convite foi cancelado. \nObrigado pela paciência...")
print("\n\t\t", guests)

pop3 = guests.pop(1)
print("\n\nDesculpe-me", pop3.title(), ",seu convite foi cancelado. \nObrigado pela paciência...")
print("\n\t\t", guests)

pop4 = guests.pop(2)
print("\n\nDesculpe-me", pop4.title(), ",seu convite foi cancelado. \nObrigado pela paciência...")
print("\n\t\t", guests)

print("\n\n\t\t\t...")

print("\n\nCaro", guests[0].title(), ", Obrigado pela paciência! \nVocê e mais um continuam convidados ao jantar! \nEspero ansiosamente! \nAtenciosamente.")
print("\nCara", guests[1].title(), ", Obrigado pela paciência! \nVocê e mais um continuam convidados ao jantar! \nEspero ansiosamente! \nAtenciosamente.")

del guests[0]
del guests[0]

print("\n\n\n\t\t", guests)
