#Nota 3


#Atividades

#3.1
   onion_quant = 5
   onion_price = 2
   result = onion_price * onion_quant

   print ("Você vai pagar", result, "pelas cebolas!")


#3.2
    # a variável cebolas recebe o valor inteiro de 300.
   cebolas = 300
   # a variável cebolas_na_caixa recebe o valor inteiro de 120.
   cebolas_na_caixa = 120
   # a variável espaco_caixa recebe o valor inteiro de 5.
   espaco_caixa = 5
   # a variável caixas recebe o valor inteiro de 60.
   caixas = 60
   # a variável cebolas_fora_da_caixa recebe o valor inteiro da
   # operação cebola (300) menos cebolas_na_caixa (120)
   cebolas_fora_da_caixa = cebolas ­ cebolas_na_caixa
   # caixas_vazias recebe o valor do resultado da operação
   caixas_vazias = caixas – (cebolas_na_caixa/espaco_caixa)
   # caixas_necessarias recebe o valor do resultado da operação
   caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
   # em teoria, esta linha deveria mostrar na tela a concatenação
   # entre as strings e as variáveis a seguir
   print "Existem", cebolas_na_caixa, "cebolas encaixotadas"
   # em teoria, esta linha deveria mostrar na tela a concatenação
   # entre as strings e as variáveis a seguir
   print "Existem", cebolas_fora_da_caixa, "cebolas sem caixa"
   # em teoria, esta linha deveria mostrar na tela a concatenação
   # entre as strings e as variáveis a seguir
   print "Em cada caixa cabem", espaco_caixa, "cebolas"
   # em teoria, esta linha deveria mostrar na tela a concatenação
   # entre as strings e as variáveis a seguir
   print "Ainda temos,", caixas_vazias, "caixas vazias"
   # em teoria, esta linha deveria mostrar na tela a concatenação
   # entre as strings e as variáveis a seguir
   print "Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas"
   
   # A seguir, o código com suas devidas correções(Os erros ocorriam porque os parênteses 
   # não haviam sido colocados depois de "print".):
   
      cebolas = 300
      cebolas_na_caixa = 120
      espaco_caixa = 5
      caixas = 60
      cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
      caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
      caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
      print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
      print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
      print ("Em cada caixa cabem", espaco_caixa, "cebolas")
      print ("Ainda temos,", caixas_vazias, "caixas vazias")
      print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as  cebolas")

